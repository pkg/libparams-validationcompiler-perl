Source: libparams-validationcompiler-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Nick Morrott <knowledgejunkie@gmail.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libeval-closure-perl <!nocheck>,
                     libexception-class-perl <!nocheck>,
                     libmoose-perl <!nocheck>,
                     libscalar-list-utils-perl <!nocheck>,
                     libspecio-perl <!nocheck>,
                     libtest-simple-perl <!nocheck>,
                     libtest-without-module-perl <!nocheck>,
                     libtest2-plugin-nowarnings-perl <!nocheck>,
                     libtest2-suite-perl <!nocheck>,
                     libtype-tiny-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libparams-validationcompiler-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libparams-validationcompiler-perl.git
Homepage: https://metacpan.org/release/Params-ValidationCompiler
Rules-Requires-Root: no

Package: libparams-validationcompiler-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libeval-closure-perl,
         libexception-class-perl,
         libscalar-list-utils-perl
Recommends: libclass-xsaccessor-perl
Description: module to build an optimized subroutine parameter validator
 Params::ValidationCompiler creates a customized, highly efficient parameter
 checking subroutine. It can handle named or positional parameters, and can
 return the parameters as key/value pairs or a list of values.
 .
 In addition to type checks, it also supports parameter defaults, optional
 parameters, and extra "slurpy" parameters.
 .
 A parameter specification can be provided as either an arrayref (which expects
 positional params) or a hashref (which expects named params). Parameter types
 can be specified as Moose, Type::Tiny or Specio type objects. Parameters can
 be specified as being optional and can be provided with default values.
