libparams-validationcompiler-perl (0.31-1+apertis1) apertis; urgency=medium

  * Sync updates from debian/bookworm
  * Do not b-d on libtype-tiny-perl, it increases a lot the deps chain

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 18 Apr 2023 19:33:03 +0530

libparams-validationcompiler-perl (0.31-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.31.
  * Update years of upstream copyright.
  * Update alternative dependencies.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Jan 2023 22:07:38 +0100

libparams-validationcompiler-perl (0.30-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.2.1, no changes needed.

  [ gregor herrmann ]
  * Update lintian overrides for renamed tags.

  [ Debian Janitor ]
  * Remove 1 unused lintian overrides.
  * Remove constraints unnecessary since buster
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 04 Dec 2022 23:35:59 +0000

libparams-validationcompiler-perl (0.30-1co2) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:24:16 +0000

libparams-validationcompiler-perl (0.30-1co1) apertis; urgency=medium

  * Do not b-d on libtype-tiny-perl, it increases a lot the deps chain

 -- Lucas Kanashiro <lucas.kanashiro@collabora.com>  Tue, 26 Feb 2019 11:49:48 -0300

libparams-validationcompiler-perl (0.30-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.30.
  * Install new Code of Conduct document.
  * Add libclass-xsaccessor-perl to Recommends.
  * Add libmoose-perl to Build-Depends-Indep. Used in an optional test.
  * Declare compliance with Debian Policy 4.2.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 05 Aug 2018 18:01:56 +0200

libparams-validationcompiler-perl (0.27-1) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.27.
  * Update years of upstream copyright.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Mar 2018 18:05:37 +0100

libparams-validationcompiler-perl (0.26-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.2 (no changes needed)

  [ Nick Morrott ]
  * New upstream version 0.26
  * Update build-deps (internal switch to Test2::V0)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Thu, 21 Dec 2017 21:07:23 +0000

libparams-validationcompiler-perl (0.24-1) unstable; urgency=medium

  * Team upload.
  * Drop debian/tests/pkg-perl/smoke-tests, handled by pkg-perl-
    autopkgtest now.
  * Import upstream version 0.24.
  * Declare compliance with Debian Policy 4.0.0.
  * Change order of alternative build dependencies.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Aug 2017 23:51:46 -0400

libparams-validationcompiler-perl (0.23-1) unstable; urgency=medium

  * Import upstream version 0.23
  * debian/copyright: update years of copyright

 -- Nick Morrott <knowledgejunkie@gmail.com>  Wed, 25 Jan 2017 00:26:56 +0000

libparams-validationcompiler-perl (0.22-1) unstable; urgency=medium

  * Import upstream version 0.22

 -- Nick Morrott <knowledgejunkie@gmail.com>  Wed, 04 Jan 2017 06:40:13 +0000

libparams-validationcompiler-perl (0.21-1) unstable; urgency=medium

  * Import upstream version 0.21
  * debian/upstream/metadata: update bug reporting URL

 -- Nick Morrott <knowledgejunkie@gmail.com>  Wed, 21 Dec 2016 20:25:42 +0000

libparams-validationcompiler-perl (0.19-1) unstable; urgency=medium

  * Import upstream version 0.19

 -- Nick Morrott <knowledgejunkie@gmail.com>  Wed, 23 Nov 2016 10:02:40 +0000

libparams-validationcompiler-perl (0.18-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.18.
  * Update (build) dependencies.
  * Update long description from upstream's improved documentation.

 -- gregor herrmann <gregoa@debian.org>  Tue, 15 Nov 2016 20:47:45 +0100

libparams-validationcompiler-perl (0.13-1) unstable; urgency=low

  * Initial Release. (Closes: #838612)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Tue, 11 Oct 2016 03:18:51 +0100
